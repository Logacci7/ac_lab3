from isa import Opcode

PROGRAM_SEC = 'section .program:'
DATA_SEC = 'section .data:'

OPCODES = {
    'add': 0x0,
    'addi': 0x40000000,
    'sub': 0x4000000,
    'mul': 0xC000000,
    'div': 0x8000000,
    'and': 0x10000000,
    'andi': 0x44000000,
    'or': 0x14000000,
    'ori': 0x48000000,
    'beq': 000000,
    'j': 0x80000000,
    'load': 0xC0000000,
    'store': 0xC4000000,
    'halt': 0x3F
}

B_REGISTER = {
    'r0': 0x1,
    'r1': 0x2,
    'r2': 0x3,
    'r3': 0x4,
    'rhi': 0x5,
    'rlo': 0x6,
}


class DataSection:
    def __init__(self, data: dict[str, int], memory: [int]):
        self.data = data
        self.memory = memory


class Operand:

    def __init__(self, value):
        self.value = value

    def translate_to_binary(self, position=0) -> int:
        return int(self.value)


class Address(Operand):

    def __init__(self, value):
        super().__init__(value)

    def get_address(self):
        return self.value

    def set_address(self, value):
        self.value = value

    def __str__(self):
        return f"ADDRESS VALUE: {self.value}; "


class Constant (Operand):

    def __init__(self, value):
        super().__init__(value)

    def __str__(self):
        return f"CONSTANT VALUE: {self.value}; "


class Register(Operand):

    def __init__(self, value):
        super().__init__(value)

    def translate_to_binary(self, position=0) -> int:
        bits_offset = (3 - position) * 5 + 11

        return B_REGISTER[self.value] << bits_offset

    def __str__(self):
        return f"REGISTER NAME: {self.value}; "


class Instruction:

    def __init__(self, opcode: Opcode, operands: []):
        self.opcode = opcode
        self.operands = operands


class ProgramSection:

    def __init__(self, labels: dict[str, int], instructions: [Instruction]):
        self.labels = labels
        self.instructions = instructions


class CodeStructure:

    def __init__(self, data: DataSection, program: ProgramSection):
        self.data_section = data
        self.program_section = program
