import re
import sys
from isa import Opcode
from exceptions import UnexpectedNameException, UnexpectedOperandException,\
    UnexpectedDataTermException, UnresolvedLabelException

from translation_module import (PROGRAM_SEC, DATA_SEC, CodeStructure,
                                ProgramSection, Instruction, Register,
                                Address, Constant, OPCODES, DataSection)


def is_comment(line: str) -> bool:
    return not line.__contains__('//')


def remove_spaces(line: str) -> str:
    return line.strip()


def preprocess(text: str) -> str:
    """
    Препроцессинг
        - Удаляем пробелы
        - Удаляем комментарии
        - Удаляем пустые строки
    """

    lines = text.split('\n')
    ns_lines = map(remove_spaces, lines)
    nc_lines = filter(is_comment, ns_lines)

    result = list(filter(bool, nc_lines))
    return '\n'.join(result)


def parse_data(lines: list[str]) -> DataSection:
    """
    Трансляция сектора данных в структуру DataSection
    """

    data: dict[str, int] = {}
    memory: [int] = [0, 0, 0]
    line_offset = 0

    # memory - наполняется значениями переменных
    # data - наполняется именами переменных и их адресами
    for line_num, line in enumerate(lines):
        var, value = map(str.strip, line.split(':'))
        if value.isdigit():
            data[var] = line_num + line_offset
            memory.append(value)

        elif value.startswith('\''):
            cut_value = value[1:len(value) - 1]
            data[var] = line_num + line_offset

            chars = list(map(ord, cut_value))
            memory.extend(chars)

            line_offset += len(chars) - 1

        elif value.startswith('array'):
            _, length = value.split(' ')
            init_array = [0] * int(length)
            data[var] = line_num + line_offset

            memory.extend(init_array)

            line_offset += int(length) - 1
        else:
            raise UnexpectedDataTermException(f'Wrong data declaration: {value}, '
                                              f'line {line_num + 1} at section .data')
    return DataSection(data, memory)


def parse_program(lines: list[str], data: DataSection) -> ProgramSection:
    """
    Трансляция сектора данных в структуру ProgramSection
    """

    labels: dict[str, int] = {}
    instructions: [Instruction] = []

    label_mark: dict[str, int] = {}
    real_operations = 0

    # Первый проход - трансляция в последовательность значимых термов
    for line_num, line in enumerate(lines):
        opcode, _, operands = line.partition(' ')
        if opcode in OPCODES.keys():
            operands_terms = map(str.strip, operands.split(','))
            operands_list = []
            for term in operands_terms:
                if len(term) == 0:
                    break
                if term.startswith('$'):
                    pure_register = term[1:len(term)]
                    operands_list.append(Register(pure_register))

                elif term.isdigit():
                    operands_list.append(Constant(term))

                elif re.match(r'[a-zA-Z0-9_]+\[\d+]', term):
                    index = int(re.findall(r'\d+', term)[-1])
                    nb_term = term[0: term.find('[')]
                    var_address = data.data[nb_term]
                    operands_list.append(Address(var_address + index))

                elif re.match(r'[a-zA-Z0-9]+', term):
                    address = data.data[term]
                    operands_list.append(Address(address))

                elif term.startswith('.'):
                    label_mark[term] = real_operations
                    operands_list.append(Address(-1))

                else:
                    raise UnexpectedOperandException(f'Incorrect operand: {opcode}, '
                                                     f'line: {line_num + 1} at section .program')

            instructions.append(Instruction(Opcode(opcode), operands_list))
            real_operations += 1
        elif opcode.startswith('.'):
            trim_opcode = opcode[0:len(opcode) - 1]
            labels[trim_opcode] = real_operations

        else:
            raise UnexpectedNameException(f'Incorrect instruction {opcode},'
                                          f' line: {line_num + 1} at section .program')

    # Второй проход - заглушки адресов меток заменяются реальными адресами
    for label, address in label_mark.items():
        if label not in labels.keys():
            raise UnresolvedLabelException(f'Unexpected label: {label}')
        instructions[address].operands[0].set_address(labels[label])

    return ProgramSection(labels, instructions)


def translate_to_structure(text: str) -> CodeStructure:
    """
    Трансляция исходного текста в структуру CodeStructure
    """

    # Препроцессинг
    proc_lines = preprocess(text)

    # Разбиваем текст на секторы данных и программы
    program_section_start = proc_lines.find(PROGRAM_SEC)
    data_sec_str = proc_lines[len(DATA_SEC):program_section_start].strip()
    program_sec_str = proc_lines[program_section_start + len(PROGRAM_SEC) + 1:len(proc_lines) + 1]

    # Транслируем
    data_section = parse_data(data_sec_str.split('\n'))
    program_section = parse_program(program_sec_str.split('\n'), data_section)

    return CodeStructure(data_section, program_section)


def text_program_structure(program: ProgramSection) -> str:
    """
    Трансляция структуры программы в текстовое представление
    """

    lines = ["PROGRAM SECTION:"]
    for line_num, instruction in enumerate(program.instructions):
        line = "PROGRAM MEMORY ADDRESS: {0}; OPCODE: {1}; ".format(line_num, instruction.opcode)
        for operation in instruction.operands:
            line += str(operation)
        lines.append(line)

    return '\n'.join(lines)


def text_data_structure(data: DataSection) -> str:
    """
    Трансляция структуры данных в текстовое представление
    """

    lines: [str] = ["DATA SECTION:"]
    for line_num, memory_line in enumerate(data.memory):
        line = "DATA MEMORY ADDRESS: {0}; MEMORY VALUE: {1}".format(line_num, memory_line)
        lines.append(line)
    lines.append('\n')
    return '\n'.join(lines)


def write_translated_code(code: CodeStructure, file: str) -> None:
    """
    Трансляция полной структуры кода (CodeStructure) в текст + запись в файл
    """

    lines = text_data_structure(code.data_section)
    lines += text_program_structure(code.program_section)

    with open(file, "w", encoding="utf-8") as f:
        f.write(lines)


def translate_to_bin_code(code: CodeStructure, target: str):
    """
    Трансляция полной структуры кода (CodeStructure) в бинарное предстваление + запись в файл
    """
    bin_code = []
    for data_line in code.data_section.memory:
        bin_code.append(str(data_line))
    bin_code.append(str(0xffff))

    for code_line in code.program_section.instructions:
        bin_opcode = OPCODES[code_line.opcode]
        bin_operands = []
        registers_count = 1

        for operand in code_line.operands:
            is_register = type(operand) == Register
            position = registers_count if is_register else 0

            bin_operands.append(operand.translate_to_binary(position))

            if is_register:
                registers_count += 1

        bin_code.append(str(bin_opcode + sum(bin_operands)))

    with open(target, "w", encoding="utf-8") as f:
        f.write('\n'.join(bin_code))


def main(args):
    assert len(args) == 3, \
        "Wrong number of arguments"

    source, target_text, target_bin = args
    with open(source, "rt", encoding="utf-8") as f:
        source = f.read()

    code = translate_to_structure(source)
    write_translated_code(code, target_text)
    translate_to_bin_code(code, target_bin)


if __name__ == '__main__':
    main(sys.argv[1:])
